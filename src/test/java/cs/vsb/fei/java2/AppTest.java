package cs.vsb.fei.java2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class AppTest {

	Person p = Person.builder("John", "Doe")
			.age(30)
			.email("john.doe@example.com").build();
	Person p2 = Person.builder("Marry", "Jane")
			.age(35)
			.gender(Gender.FEMALE).build();
	Person p3 = Person.builder("John", "Doe")
			.age(30)
			.email("john.doe@example.com").build();

	PersonWithAPhone pwp = (PersonWithAPhone) PersonWithAPhone.builder("Dominik", "Slizák")
			.phoneNumber("123 456 789")
			.age(30)
			.email("dominik.slizak@seznam.cz")
			.gender(Gender.MALE).build();

	PersonWithAPhone pwp2 = (PersonWithAPhone) PersonWithAPhone.builder("John", "Doe")
			.age(30)
			.email("john.doe@example.com").build();

	PersonWithAPhone pwp3 = (PersonWithAPhone) PersonWithAPhone.builder("John", "Doe")
			.age(30)
			.email("john.doe@example.com").build();


	@Test
	void testPersonEqualsSameObject() {
		assertTrue(p.equals(p));
	}

	@Test
	void testPWPEqualsSameObject() {
		assertTrue(pwp.equals(pwp));
	}

	@Test
	void testPersonEqualsNull() {
		assertFalse(p.equals(null));
	}

	@Test
	void testPWPEqualsNull() {
		assertFalse(pwp.equals(null));
	}

	@Test
	void testPersonEqualsPWP() {
		assertFalse(p.equals(pwp2));
	}

	@Test
	void testPersonEqualsSameValues() {
		assertTrue(p.equals(p3));
	}

	@Test
	void testPWPEqualsSameValues() {
		assertTrue(pwp2.equals(pwp3));
	}

	@Test
	void testPersonEqualsDifferentPerson() {
		assertFalse(p.equals(p2));
	}

	@Test
	void testPWPEqualsDifferentPWP() {
		assertFalse(pwp.equals(pwp3));
	}

}
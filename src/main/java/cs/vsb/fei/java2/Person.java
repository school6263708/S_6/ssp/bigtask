package cs.vsb.fei.java2;

import lombok.ToString;

@ToString
public class Person {
    protected final String firstName;
    protected final String lastName;
    protected final int age;
    protected final String email;
    protected final Status status;
    protected final Gender gender;

    protected Person(Builder builder) {
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.age = builder.age;
        this.email = builder.email;
        this.status = builder.status;
        this.gender = builder.gender;
    }

    public static Builder builder(String firstName, String lastName) {
        return new Builder(firstName, lastName);
    }

    public static class Builder {
        protected String firstName;
        protected String lastName;
        protected int age;
        protected String email;
        protected Status status;
        protected Gender gender;

        public Builder(String firstName, String lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public Builder age(int age) {
            this.age = age;
            return this;
        }

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public Builder status(Status status) {
            this.status = status;
            return this;
        }

        public Builder gender(Gender gender) {
            this.gender = gender;
            return this;
        }

        public Builder phoneNumber(String phoneNumber) {
            return this;
        }

        public Person build() {
            return new Person(this);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Person person = (Person) obj;
        return firstName.equals(person.firstName) &&
                lastName.equals(person.lastName) &&
                age == person.age &&
                email != null ? email.equals(person.email) : person.email == null &&
                status == person.status &&
                gender == person.gender;
    }
}

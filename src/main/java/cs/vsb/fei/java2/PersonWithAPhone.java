package cs.vsb.fei.java2;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString(callSuper = true)
public class PersonWithAPhone extends Person {
    private final String phoneNumber;

    protected PersonWithAPhone(Builder builder) {
        super(builder);
        this.phoneNumber = builder.phoneNumber;
    }

    public static Builder builder(String firstName, String lastName) {
        return new Builder(firstName, lastName);
    }

    public static class Builder extends Person.Builder {
        protected String phoneNumber;

        public Builder(String firstName, String lastName) {
            super(firstName, lastName);
        }

        @Override
        public Builder phoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        @Override
        public PersonWithAPhone build() {
            return new PersonWithAPhone(this);
        }

    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;
        PersonWithAPhone person = (PersonWithAPhone) obj;
        return phoneNumber != null ? phoneNumber.equals(person.phoneNumber) : person.phoneNumber == null;
    }
}

package cs.vsb.fei.java2;

import lombok.extern.log4j.Log4j2;

/**
 *  Class <b>App</b> - main class
 *  @author     Java I
 */
@Log4j2
public class App {

	public static void main(String[] args) {

		log.info("Launching Java application.");

		Person p = Person
				.builder("John", "Doe")
				.age(30)
				.email("john.doe@seznam.cz")
				.status(Status.MARRIED).build();

		PersonWithAPhone pwp = (PersonWithAPhone) PersonWithAPhone.builder("Dominik", "Slizák")
				.phoneNumber("123 456 789")
				.age(30)
				.email("dominik.slizak@seznam.cz")
				.gender(Gender.MALE).build();

		log.info(p.toString());
		log.info(pwp.toString());


//		PersonWithAPhone pwp2 = PersonWithAPhone.builder("Dominik", "Slizák")
//				.age(30)
//				.email("dominik.slizak@seznam.cz")
//				.phoneNumber("123 456 789")
//				.build();

	}
	
}
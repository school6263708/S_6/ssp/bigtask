package cs.vsb.fei.java2;

public enum Status {
    MARRIED, SINGLE, DIVORCED, WIDOWED
}

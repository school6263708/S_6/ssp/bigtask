[//]: # (* TODO: Create the template )
* [ ] Verify all unit tests have passed.
* [ ] Create README and add info about the project.
* [ ] Update README with the newest changes.
* [ ] Add project badges into it:
  * [ ] CI status
* [ ] Try to add HTML status about unit tests

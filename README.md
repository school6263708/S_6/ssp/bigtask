# Závěrečná práce

![pipeline](https://gitlab.com/school6263708/S_6/ssp/bigtask/badges/main/pipeline.svg)

* Tento projekt slouží jako "šablona" pro `JAVA` projekty a demonstraci schopností
naučených v kurzu `SSP`.

## CHECKSTYLE

* `Checkstyle` je vývojový nástroj pro Javu, jenž pomáhá dodržovat dobré zásady
psaní kódu.
* Konfigurace, jakým způsobem se má kód kontrolovat se nachází v souboru `checkstyle.xml`.

## PYMARKDOWN

* `Pymarkdown` je open source projekt, jenž se stará o kontrolu `.md` souborů.
* Celou knihovnu lze najít na GitHubu: [pymarkdown](https://github.com/jackdewinter/pymarkdown).

## PMD

* TODO: najít informace o tomto pluginu.

## Kontinuální integrace

* Konfiguraci kontinuální integrace se nachází v souboru `.gitlab-ci.yml`.
* Momentální konfigurace se skládá z těchto "prací":
  * `testSyntaxMD` - Tato práce má za úkol otestovat syntaxi `README` souboru.
  * `testSyntaxJava` - Tato práce má za úkol zkontrolovat syntaxi na základě
  konfigurace v`checkstyle.xml`.
  * `mavenBuild` - Tato práce má za úkol zkontrolovat, zda se projekt úspěšně sestaví.
  * `mavenTest` - Tato práce má za úkol provést `UNIT` testy, které se nachází
  ve složce `/src/test/java/cs/vsb/fei/java2`.
  * `checkSecurityUpdatesPython` - Tato práce má za úkol zkontrolovat, zda jsou všechny
  závislosti jazyka `Python` aktuální a bezpečné.
  * `checkSecurityUpdatesJava` - Tato práce má za úkol zkontrolovat, zda jsou všechny
  závislosti jazyka `Java` aktuální a bezpečné.
  * `pages` - Tato práce má za úkol nahrát report z předchozí práce.

## Instalace

* Instalace je velmi jednoduchá a skládá se z následujících kroků:
  1. Naklonovat prostředí: `git clone git@gitlab.com:school6263708/S_6/ssp/bigtask.git`.
  2. Odstranit všechny vygenerované soubory ve složce target: `mvn clean`.
  3. Provést kompilaci a spuštění testů pomocí příkazu `mvn package`.
  4. Následně stačí projekt spustit.

## Report

* Report lze nalézt na adrese: [report](https://bigtask-school6263708-s-6-ssp-51c7a896f065c14a7aed3fef5730496bb.gitlab.io/).

## Projekt vytvořili

* Radek Šmiga - SMI0132
* Eliška Malcharcziková - MAL0404
